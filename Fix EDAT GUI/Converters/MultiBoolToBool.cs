﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Fix_EDAT_GUI.Converters
{
    public class MultiBoolToBool : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool? canNew = values[0] as bool?;
            bool? isNewChecked = values[1] as bool?;

            if (canNew.HasValue && isNewChecked.HasValue && canNew.Value && isNewChecked.Value)
                return true;

            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
