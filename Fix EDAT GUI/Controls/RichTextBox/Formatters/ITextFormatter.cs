﻿using System.Windows.Documents;

namespace Extra.Controls
{
    public interface ITextFormatter
    {
        string GetText(FlowDocument document);

        void SetText(FlowDocument document, string text);
    }
}
