﻿using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Fix_EDAT_GUI.Models
{
    public class EDATInfo : INotifyPropertyChanged
    {
        #region Fields

        private bool canDecryptToDebug, canNewCID, canNewType, decryptToDebug, isNewTypeChecked, isNewCIDChecked;

        private string log, newCID, newType, parameters, target;

        private int progress;

        #endregion Fields

        #region Properties

        public bool CanDecryptToDebug
        {
            get { return canDecryptToDebug; }
            set
            {
                if (canDecryptToDebug == value) return;
                canDecryptToDebug = value;
                NotifyPropertyChanged("CanDecryptToDebug");
            }
        }

        public bool CanNewCID
        {
            get { return canNewCID; }
            set
            {
                if (canNewCID == value) return;
                canNewCID = value;
                NotifyPropertyChanged("CanNewCID");
            }
        }

        public bool CanNewType
        {
            get { return canNewType; }
            set
            {
                if (canNewType == value) return;
                canNewType = value;
                NotifyPropertyChanged("CanNewType");
            }
        }

        public bool DecryptToDebug
        {
            get { return decryptToDebug; }
            set
            {
                if (decryptToDebug == value) return;
                decryptToDebug = value;
                NotifyPropertyChanged("DecryptToDebug");

                CanNewCID = !value;
                CanNewType = !value;
            }
        }

        public bool IsNewCIDChecked
        {
            get { return isNewCIDChecked; }
            set
            {
                if (isNewCIDChecked == value) return;
                isNewCIDChecked = value;
                NotifyPropertyChanged("IsNewCIDChecked");
            }
        }

        public bool IsNewCIDValid
        {
            get { return CanNewCID && IsNewCIDChecked && !string.IsNullOrWhiteSpace(NewCID) && Regex.IsMatch(NewCID, @"[A-Z]{2}\d{4}\-[A-Z]{4}\d{5}_\d{2}\-[\S]{16}"); }
        }

        public bool IsNewTypeChecked
        {
            get { return isNewTypeChecked; }
            set
            {
                if (isNewTypeChecked == value) return;
                isNewTypeChecked = value;
                NotifyPropertyChanged("IsNewTypeChecked");
            }
        }

        public string Log
        {
            get { return log; }
            set
            {
                if (log == value) return;
                log = value;
                NotifyPropertyChanged("Log");
            }
        }

        public string NewCID
        {
            get { return newCID; }
            set
            {
                if (newCID == value) return;
                newCID = value;
                NotifyPropertyChanged("NewCID");
            }
        }

        public string NewType
        {
            get { return newType; }
            set
            {
                if (newType == value) return;
                newType = value;
                NotifyPropertyChanged("NewType");
            }
        }

        public string Parameters
        {
            get { return "-" + parameters; }
            set
            {
                if (parameters == value) return;
                parameters = value;
                NotifyPropertyChanged("Parameters");
            }
        }

        public int Progress
        {
            get { return progress; }
            set
            {
                if (progress == value) return;
                progress = value;
                NotifyPropertyChanged("Progress");
            }
        }

        public string Target
        {
            get { return target; }
            set
            {
                if (target == value) return;
                target = value;
                NotifyPropertyChanged("Target");
            }
        }

        public string[] Types { get { return new string[] { "0", "1", "20", "30" }; } }

        #endregion Properties

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
