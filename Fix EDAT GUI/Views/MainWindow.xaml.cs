﻿using Extra.Controls;
using Fix_EDAT_GUI.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Fix_EDAT_GUI.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields

        private static Process process = null;
        private object @lock = new object();
        private string fixEdat, logsDirectory;

        #endregion Fields

        #region Properties

        public EDATInfo EDATInfo { get; private set; }

        #endregion Properties

        #region Constructor

        public MainWindow()
        {
            fixEdat = Path.Combine(Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName, "fix_edat.exe");

            logsDirectory = Path.Combine(Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName, "Logs");

            if (!Directory.Exists(logsDirectory))
                Directory.CreateDirectory(logsDirectory);

            EDATInfo = new EDATInfo();

            InitializeComponent();

            if (!File.Exists(fixEdat))
                EDATInfo.Log += "<h2><font color=\"red\">fix_edat not found</font></h2>";
        }

        #endregion Constructor

        #region CanExecute

        private void CheckUpdates_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists(Path.Combine(Environment.CurrentDirectory, "Updater.exe"));
        }

        private void FixEDAT_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists(fixEdat) && !string.IsNullOrEmpty(EDATInfo.Target) && (!EDATInfo.CanNewCID || EDATInfo.CanNewCID && !EDATInfo.IsNewCIDChecked || EDATInfo.IsNewCIDValid);
        }

        private void FixEDATExists_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists(fixEdat);
        }

        #endregion CanExecute

        #region Executed

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            MessageBox.Show(string.Format("Fix EDAT GUI v{0} by Dasanko.", thisAssembly.GetName().Version), "Fix EDAT GUI", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void CheckUpdates_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            Process.Start(Path.Combine(Environment.CurrentDirectory, "Updater.exe"), string.Format("\"{0}\" \"{1}\" \"{2}\"", thisAssembly.GetName().Name, thisAssembly.GetName().Version, Process.GetCurrentProcess().MainModule.FileName));
        }

        private void ClearLog_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            EDATInfo.Log = string.Empty;
        }

        private void FixEDAT_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string parameters = ConfigureParameters();

            Task.Factory.StartNew(() =>
            {
                string logFilename = string.Format("{0}-{1}-{2}.{3}{4}{5}.txt", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.TimeOfDay.Hours, DateTime.Now.TimeOfDay.Minutes, DateTime.Now.TimeOfDay.Seconds);

                if (Directory.Exists(EDATInfo.Target))
                    foreach (var edat in Directory.EnumerateFiles(EDATInfo.Target, "*.edat", SearchOption.AllDirectories))
                        process = FixEDAT(string.Format("\"{0}\" {1}", edat, parameters));
                else
                    process = FixEDAT(string.Format("\"{0}\" {1}", EDATInfo.Target, parameters));

                while (process != null && !process.HasExited)
                    Thread.Sleep(100);

                process.Dispose();

                IEnumerable<string> allFiles;

                if (Directory.Exists(EDATInfo.Target))
                    allFiles = Directory.EnumerateFiles(EDATInfo.Target, "*.*", SearchOption.AllDirectories);
                else
                    allFiles = Directory.EnumerateFiles(Directory.GetParent(EDATInfo.Target).FullName, Path.GetFileNameWithoutExtension(EDATInfo.Target) + ".*", SearchOption.TopDirectoryOnly);

                int edats = allFiles.Where(x => x.EndsWith(".edat", StringComparison.InvariantCultureIgnoreCase)).Count();
                int backups = allFiles.Where(x => x.ToLower().Contains(".edat.bak")).Count();
                int debugs = allFiles.Where(x => x.EndsWith(".edat.dbg", StringComparison.InvariantCultureIgnoreCase)).Count();
                int aborteds = allFiles.Where(x => x.EndsWith(".edat.temp", StringComparison.InvariantCultureIgnoreCase)).Count();

                Log("edat: " + edats, logFilename);
                Log("edat backup: " + backups, logFilename);
                Log("edat debug: " + debugs, logFilename);
                Log("edat aborted: " + aborteds, logFilename);

                if (aborteds > 0)
                {
                    Log(string.Empty, logFilename);

                    foreach (var file in allFiles.Where(x => x.EndsWith(".edat.temp", StringComparison.InvariantCultureIgnoreCase)))
                        Log(file, logFilename);
                }
            });
        }

        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string selectDir = "Select this directory";

            var ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.FileName = selectDir;
            ofd.Filter = "EDAT Files (*.edat)|*.edat";
            ofd.Multiselect = false;

            if (ofd.ShowDialog().Value)
            {
                EDATInfo.Log = string.Empty;

                if (!ofd.FileName.Contains(selectDir))
                {
                    EDATInfo.Target = ofd.FileName;

                    CheckEdat(string.Format("\"{0}\" -i", ofd.FileName));
                }
                else
                {
                    EDATInfo.Target = ofd.FileName.Split(new string[] { selectDir }, StringSplitOptions.None)[0];

                    EDATInfo.CanDecryptToDebug = true;
                    EDATInfo.CanNewCID = true;
                    EDATInfo.CanNewType = true;
                }
            }
        }

        #endregion Executed

        #region Events

        private void CheckEDAT_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            string message = e.Data + "<br/>";

            if (message.StartsWith("WARNING") || message.StartsWith("cid: empty"))
                message = "<font color=\"red\">" + message + "</font>";

            EDATInfo.CanDecryptToDebug = message.Contains("status:") && !message.Contains("debug") ? true : false;

            EDATInfo.CanNewCID = true;
            EDATInfo.CanNewType = true;

            if (message.Contains("license:") && message.Contains("empty"))
                EDATInfo.IsNewCIDChecked = true;
            else if (message.Contains("license:") && !message.Contains("empty"))
            {
                EDATInfo.IsNewCIDChecked = false;

                CommandManager.InvalidateRequerySuggested();
            }

            EDATInfo.Log += message;
        }

        private void FixEDAT_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            string message = e.Data + "<br/>";

            if (message.StartsWith("ERROR"))
                message = "<font color=\"red\">" + message + "</font>";

            if (message.Contains("%"))
            {
                int start = message.IndexOf('[') + 1;
                int end = message.IndexOf('%');

                if (end - start < 0)
                    return;

                message = message.Substring(start, end - start).Trim();

                EDATInfo.Progress = int.Parse(message);

                return;
            }

            EDATInfo.Log += message;
        }

        private void RichTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            var rtb = (sender as RichTextBox);

            // Only autoscroll if the scrollbar is at the bottom
            if (rtb.VerticalOffset + rtb.ViewportHeight >= rtb.ExtentHeight - 1.0)
                rtb.ScrollToEnd();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (process != null && !process.HasExited)
                process.Kill();
        }

        #endregion Events

        #region Private Methods

        private void CheckEdat(string parameters)
        {
            using (var process = new Process())
            {
                process.OutputDataReceived += CheckEDAT_OutputDataReceived;

                process.StartInfo = new ProcessStartInfo(fixEdat, parameters);
                process.StartInfo.WorkingDirectory = Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();

                process.BeginOutputReadLine();
            }
        }

        private string ConfigureParameters()
        {
            if (EDATInfo.CanDecryptToDebug && EDATInfo.DecryptToDebug)
                return "-d";

            var parameters = new StringBuilder("-f");

            if (EDATInfo.IsNewCIDValid)
                parameters.Append("r");

            if (EDATInfo.CanNewType && EDATInfo.IsNewTypeChecked)
                parameters.Append("t");

            if (EDATInfo.IsNewCIDValid)
                parameters.Append(" " + EDATInfo.NewCID);

            if (EDATInfo.CanNewType && EDATInfo.IsNewTypeChecked)
                parameters.Append(" " + EDATInfo.NewType);

            return parameters.ToString();
        }

        private Process FixEDAT(string parameters)
        {
            var process = new Process();
            process.OutputDataReceived += FixEDAT_OutputDataReceived;

            process.StartInfo = new ProcessStartInfo(fixEdat, parameters);
            process.StartInfo.WorkingDirectory = Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.Start();

            process.BeginOutputReadLine();

            return process;
        }

        private void Log(string message, string logFilename)
        {
            EDATInfo.Log += message + "<br/>";

            string logFilePath = Path.Combine(logsDirectory, logFilename);

            File.AppendAllText(logFilePath, message + Environment.NewLine);
        }

        private void NewCIDCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke((Action)delegate
            {
                if (EDATInfo.IsNewCIDChecked)
                    NewCIDTextBox.Focus();
            });
        }

        #endregion Private Methods
    }
}
